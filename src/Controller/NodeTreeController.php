<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

use App\Entity\NodeTree;

use App\Form\NodeTreeType;

/**
* NodeTree controller.
* @Route("/api", name="api_")
*/
class NodeTreeController extends FOSRestController
{
    /**
     * Lists all NodeTrees.
     *
     * @Rest\Get("/node-trees")
     * @return Response
     *
     * @SWG\Tag(name="NodeTree")
     * 
     * @SWG\Response(
     *   response=200,
     *     description="Returns node tree list",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=NodeTree::class, groups={"full"}))
     *     )
     * )
     */
    public function getNodeTreeAction()
    {
        $repository = $this->getDoctrine()->getRepository(NodeTree::class);
        $node_trees = $repository->findAllTrees();
        return $this->handleView($this->view($node_trees), Response::HTTP_OK);
    }
    
    /**
     * Create NodeTree.
     * 
     * @Rest\Post("/node-trees")
     * @return Response
     * 
     * @SWG\Tag(name="NodeTree")
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         @SWG\Schema(ref=@Model(type=NodeTreeType::class))
     * ),
     * @SWG\Response(
     *     response=201,
     *     description="Returns new created node tree",
     *     @Model(type=NodeTree::class, groups={"non_sensitive_data"})
     * )
     */
    public function postNodeTreeAction(Request $request)
    {
        $node_tree = new NodeTree();
        $form = $this->createForm(NodeTreeType::class, $node_tree);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($node_tree);
            $em->flush();
            return $this->handleView($this->view($node_tree), Response::HTTP_CREATED);
        }
        return $this->handleView($this->view($form->getErrors()));
    }
    
    /**
     * Update NodeTree.
     * 
     * @Rest\Put("/node-trees/{node_tree_id}")
     * @Rest\Patch("/node-trees/{node_tree_id}")
     
     * @return Response
     * 
     * @SWG\Tag(name="NodeTree")
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         @SWG\Schema(ref=@Model(type=NodeTreeType::class))
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Returns updated node tree",
     *     @Model(type=NodeTree::class, groups={"non_sensitive_data"})
     * )
     */
    public function putNodeTreeAction(Request $request, $node_tree_id)
    {
        $node_tree = $this->getDoctrine()
            ->getRepository('App:NodeTree')
            ->find(intval($node_tree_id));
        if (!$node_tree) {
            throw $this->createNotFoundException(sprintf(
                'Not found "%s"',
                'NodeTree'
            ));
        }
        $form = $this->createForm(NodeTreeType::class, $node_tree);
        $data = json_decode($request->getContent(), true);
        $isPatch = $request->getMethod() != 'PATCH';
        $form->submit($data, $isPatch);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // $em->persist($node_tree);
            $em->flush();
            return $this->handleView($this->view($node_tree), Response::HTTP_RESET_CONTENT);
        }
        return $this->handleView($this->view($form->getErrors()));
    }
    
    /**
     * Delete NodeTree.
     * 
     * @Rest\Delete("/node-trees/{node_tree_id}")
     
     * @return Response
     * 
     * @SWG\Tag(name="NodeTree")
     * @SWG\Response(
     *     response=200,
     *     description="Nil"
     * )
     */
    public function delteNodeTreeAction(Request $request, $node_tree_id)
    {
        $node_tree = $this->getDoctrine()
            ->getRepository('App:NodeTree')
            ->find(intval($node_tree_id));
        if (!$node_tree) {
            throw $this->createNotFoundException(sprintf(
                'Not found "%s"',
                'NodeTree'
            ));
        }
        try{
            $em = $this->getDoctrine()->getManager();
            $em->remove($node_tree);
            $em->flush();
            return $this->handleView($this->view('', Response::HTTP_NO_CONTENT));
        } catch (Exception $e) {}
        return $this->handleView($this->view('', Response::HTTP_NOT_ACCEPTABLE));
    }
}
