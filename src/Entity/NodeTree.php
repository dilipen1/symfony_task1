<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NodeTreeRepository")
 */
class NodeTree
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * 
     * @var int
     * @SWG\Property(description="The unique identifier of the node tree.")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @SWG\Property(type="string", maxLength=255)
     */
    private $title;
    
    /**
     * @ORM\OneToMany(targetEntity="NodeTree", mappedBy="parent")
     */
    private $children;

    /**
     * 
     * @Assert\NotBlank(allowNull = true)
     * 
     * @ORM\ManyToOne(targetEntity="NodeTree", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     * 
     * @SWG\Property(type="integer")
     */
    private $parent;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getParent(): ?NodeTree
    {
        return $this->parent;
    }

    public function setParent(NodeTree $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|NodeTree[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(NodeTree $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(NodeTree $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }
}
