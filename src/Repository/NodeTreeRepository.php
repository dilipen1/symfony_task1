<?php

namespace App\Repository;

use App\Entity\NodeTree;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method NodeTree|null find($id, $lockMode = null, $lockVersion = null)
 * @method NodeTree|null findOneBy(array $criteria, array $orderBy = null)
 * @method NodeTree[]    findAll()
 * @method NodeTree[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NodeTreeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NodeTree::class);
    }

    // /**
    //  * @return NodeTree[] Returns an array of NodeTree objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NodeTree
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    
    public function findAllTrees()
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.parent is NULL')
            ->orderBy('n.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
